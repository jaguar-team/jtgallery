<div id="gallery" style="display:none;">
	{foreach from=$data key=number item=value}
        <a href="#">
		<img alt="{$value.title}"
		     src="{$value.img}"
		     data-image="{$value.img}"
		     data-description="{$value.desc}"
		     style="display:none">
		</a>
    {/foreach}
</div>