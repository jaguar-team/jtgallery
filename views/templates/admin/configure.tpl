{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2015 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<div id="jtgallery" class="panel">

	<div class="panel-heading"><i class="icon icon-tags"></i> {l s='Settings' mod='jtgallery'}</div>

	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="panel-body">
				<div class="header-list-photos">
					<div class="info">
						<p><b>{l s='Quantity of photos' mod='jtgallery'}</b>: {$data|@count}</p>
						<p><b>{l s='Link to gallery' mod='jtgallery'}</b>: <a target="_blank" title="{l s='Let\'s go' mod='jtgallery'}" href="{$gallery_url}">{l s='Go to' mod='jtgallery'}</a></p>
					</div>
					<div class="actions">
						<ul>
							<li>
								<a class="add_photo toolbar_btn  pointer"><i class="process-icon-new"></i></a>
								<div>{l s='Add photo' mod='jtgallery'}</div>
							</li>
						</ul>
					</div>
				</div>
				<div class="list-photos" id="accordion" role="tablist" aria-multiselectable="true">

					{foreach from=$data key=number item=value}
						<div class="container-photo">
							<div class="container-title" role="tab" id="headingOne">
								<div class="number">{$number + 1}</div>
								<div class="img"><img src="{if $value.img}{$value.img}{else}{$src_img_not_found}{/if}" alt="{$value.title}" /></div>
								<div class="title">
									<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{$number}" aria-expanded="false" aria-controls="collapse{$number}">
										{if $value.title}{$value.title}{else}{l s='Name is not set' mod='jtgallery'}{/if}
									</a>
								</div>
								<div class="actions">
									<ul>
										<li>
											<form action="" method="post">
												<input type="hidden" name="JTGALLERY_PHOTO_ID" value="{$value.id}" />
												<input type="hidden" name="JTGALLERY_PHOTO_TOKEN" value="{$token}" />
												<button type="submit" value="1" name="submitJtgalleryDelete" class="btn btn-default">
													<i class="icon-trash"></i> {l s='Delete' mod='jtgallery'}
												</button>
											</form>
										</li>
									</ul>
								</div>
							</div>
							<div id="collapse{$number}" class="content panel-collapse collapse" role="tabpanel">

								<form action="" method="post" enctype="multipart/form-data">

									<!-- hidden -->
									<input type="hidden" name="JTGALLERY_PHOTO_ID" value="{$value.id}" />
									<input type="hidden" name="JTGALLERY_PHOTO_SRC" value="{$value.img}" />
									<input type="hidden" name="JTGALLERY_PHOTO_TOKEN" value="{$token}" />
									<!-- /hidden -->

									<!-- title -->
									<div class="form-group">
										<label for="inputName">{l s='Name' mod='jtgallery'}</label>
										<input type="text" class="form-control" id="inputName" name="JTGALLERY_PHOTO_NAME" placeholder="{l s='Enter the photo name' mod='jtgallery'}" value="{$value.title}">
									</div>
									<!-- /title -->

									<!-- desc -->
									<div class="form-group">
										<label for="inputDesc">{l s='Description' mod='jtgallery'}</label>
										<textarea id="inputDesc" name="JTGALLERY_PHOTO_DESC" placeholder="{l s='Enter the photo description' mod='jtgallery'}">{$value.desc}</textarea>
									</div>
									<!-- /desc -->

									<!-- img -->
									<div class="form-group">
										<label for="exampleInputPassword1">{l s='Photo' mod='jtgallery'}</label>
										<input name="JTGALLERY_PHOTO_IMG" type="file" />
									</div>
									<!-- /img -->

									<button type="submit" value="1" name="submitJtgallerySave" class="btn btn-default">
										<i class="process-icon-save"></i> {l s='Save' mod='jtgallery'}
									</button>
								</form>

							</div>
						</div>
					{/foreach}

				</div>
			</div>
			<div class="panel-footer">
				{$pagination}
			</div>
		</div>
	</div>
</div>
<script>
	jQuery(document).ready(function($){
		$('.add_photo').click(function(){
			var number = $('.list-photos').children().length + 1;
			console.log(number);
			$('.list-photos').prepend(`
            <div class="container-photo">
				<div class="title" role="tab" id="headingOne">
					<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse` + number + `" aria-expanded="true" aria-controls="collapse` + number + `">
						{l s='New Photo' mod='jtgallery'}
					</a>
				</div>
				<div id="collapse` + number + `" class="content panel-collapse collapse in" role="tabpanel">

				<form action="" method="post" enctype="multipart/form-data">
					<input type="hidden" name="JTGALLERY_PHOTO_TOKEN" value="{$token}" />

					<!-- title -->
							<div class="form-group">
							<label for="inputName">{l s='Name' mod='jtgallery'}</label>
							<input type="text" class="form-control" id="inputName" name="JTGALLERY_PHOTO_NAME" placeholder="Введите название фотографии" value="">
							</div>
							<!-- /title -->

							<!-- desc -->
							<div class="form-group">
							<label for="inputDesc">{l s='Description' mod='jtgallery'}</label>
							<textarea id="inputDesc" name="JTGALLERY_PHOTO_DESC" placeholder="Введите описание фотографии"></textarea>
							</div>
							<!-- /desc -->

							<!-- img -->
							<div class="form-group">
							<label for="exampleInputPassword1">{l s='Photo' mod='jtgallery'}</label>
							<input name="JTGALLERY_PHOTO_IMG" type="file" />
							</div>
							<!-- /img -->

							<button type="submit" value="1" name="submitJtgallerySave" class="btn btn-default">
							<i class="process-icon-save"></i> {l s='Save' mod='jtgallery'}
							</button>
							</form>

							</div>
							</div>
							`);
    });
});
</script>