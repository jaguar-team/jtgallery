<?php
/**
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2015 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

class Jtgallery extends Module
{
    /**
     * @var bool
     */
    protected $config_form = false;

    /**
     * @var
     */
    protected $photos;

    /**
     * @var
     */
    protected $offset;

    /**
     * @var
     */
    protected $current_page;

    /**
     * Jtgallery constructor.
     */
    public function __construct()
    {
        $this->name = 'jtgallery';
        $this->tab = 'front_office_features';
        $this->version = '1.0.0';
        $this->author = 'Jaguar-Team';
        $this->need_instance = 1;

        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Gallery on the new page');
        $this->description = $this->l('Display photos such as tiled.');

        $this->confirmUninstall = $this->l('Confirm unistall module');

        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {
        Configuration::updateValue('JTGALLERY_LIVE_MODE', false);

        include(dirname(__FILE__).'/sql/install.php');

        return parent::install() &&
            $this->registerHook('header') &&
            $this->registerHook('backOfficeHeader') &&
            $this->registerHook('displayTop');
    }

    /**
     * @return mixed
     */
    public function uninstall()
    {
        Configuration::deleteByName('JTGALLERY_LIVE_MODE');

        include(dirname(__FILE__).'/sql/uninstall.php');

        return parent::uninstall();
    }

    /**
     * Load the configuration form
     */
    public function getContent()
    {
        $output = '';
        $available = false;
        session_start();

        /** generate token */
        if (!isset($_SESSION['token'])) {
            $token = md5(md5(rand(1, mt_getrandmax()).rand(1, mt_getrandmax())));
            $_SESSION['token'] = $token;
        }
        else {
            $token = $_SESSION['token'];
            $token_input = Tools::getValue('JTGALLERY_PHOTO_TOKEN');
            $available = $token_input == $token;

            $token = md5(md5(rand(1, mt_getrandmax()).rand(1, mt_getrandmax())));
            $_SESSION['token'] = $token;
        }

        /** save photo to DB */
        if (((bool)Tools::isSubmit('submitJtgallerySave')) == true && $available) {
            if ($this->savePhoto()) {
                $output .= $this->displayConfirmation($this->l('Photos were successfully updated'));
            }
        }

        /** delete photo from DB */
        if (((bool)Tools::isSubmit('submitJtgalleryDelete')) == true && $available) {
            if ($this->deletePhoto()) {
                $output .= $this->displayConfirmation($this->l('Photo was successfully deleted'));
            }
        }

        /** Get list data */
        $this->context->smarty->assign(array(
            'src_img_not_found'     => _PS_BASE_URL_.__PS_BASE_URI__.'img/scenes/ru.jpg',
            'data'                  => $this->getPhotos(),
            'pagination'            => $this->getPagination(),
            'token'                 => $token,
            'gallery_url'           => _PS_BASE_URL_.__PS_BASE_URI__.'index.php?fc=module&module=jtgallery&controller=gallery',
        ));
        $output .= $this->context->smarty->fetch($this->local_path.'views/templates/admin/configure.tpl');

        return $output;
    }

    /**
     * Get a photos
     */
    protected function getPhotos($all = false, $limit = 15)
    {
        if ($all) {
            $sql = 'SELECT * FROM '._DB_PREFIX_.'jtgallery';

            return Db::getInstance()->ExecuteS($sql);
        }

        $offset = $this->getOffset();
        $sql = 'SELECT * FROM '._DB_PREFIX_.'jtgallery 
                ORDER BY `id` DESC 
                LIMIT '.$limit.'
                OFFSET '.$offset;

        if (!$this->photos)
            $this->photos = Db::getInstance()->ExecuteS($sql);

        return $this->photos;
    }

    /**
     * Delete photo
     */
    protected function deletePhoto()
    {
        $id = Tools::getValue('JTGALLERY_PHOTO_ID');
        $table = 'jtgallery';
        $where = 'id = '.$id;

        return Db::getInstance()->delete($table, $where);
    }

    /**
     * Save photo
     */
    protected function savePhoto()
    {
        $img = NULL;

        if (Tools::getValue('JTGALLERY_PHOTO_SRC'))
            $img = Tools::getValue('JTGALLERY_PHOTO_SRC');

        if ($_FILES['JTGALLERY_PHOTO_IMG']) {

            /** if not isset dir */
            if (!is_dir(_PS_IMG_DIR_.'jtgallery/'))
                mkdir(_PS_IMG_DIR_.'jtgallery/');

            $file = $_FILES['JTGALLERY_PHOTO_IMG'];
            $type = Tools::strtolower(Tools::substr(strrchr($file['name'], '.'), 1));
            $name = md5(md5(rand(1, mt_getrandmax()).rand(1, mt_getrandmax()).'gtgallerry')).'.'.$type;
            $dir_upload = _PS_IMG_DIR_.'jtgallery/'.$name;

            if(move_uploaded_file($file['tmp_name'], $dir_upload))
                $img = _PS_BASE_URL_.__PS_BASE_URI__.'/img/jtgallery/'.$name;
        }

        $data = array(
            'title'       =>  Tools::getValue('JTGALLERY_PHOTO_NAME'),
            'desc'        =>  Tools::getValue('JTGALLERY_PHOTO_DESC'),
            'img'         => $img,
        );

        if (Tools::getValue('JTGALLERY_PHOTO_ID')) {
            $where = 'id = '.(int)Tools::getValue('JTGALLERY_PHOTO_ID');
            $data['date_upd'] = date('Y:m:d H:m:s');
            return Db::getInstance()->update('jtgallery', $data, $where, 1);
        }
        else {
            $data['date_add'] = date('Y:m:d H:m:s');
            $data['date_upd'] = date('Y:m:d H:m:s');
            return Db::getInstance()->insert('jtgallery', $data);
        }
    }

    /**
     * Get current offset & set current page
     * @return int
     */
    protected function getOffset()
    {
        $page = (Tools::getValue('page') ? Tools::getValue('page') : 1);

        $this->offset = ($page == 1 ? 0 : ($page - 1) * 15);
        $this->current_page = $page;

        return $this->offset;
    }

    /**
     * Get pagination HTML, 15 objects per page
     * @return bool|string
     */
    protected function getPagination()
    {
        $count_photos = count($this->getPhotos(true));

        if (!$count_photos) {
            return false;
        }

        $count_pag = ceil($count_photos / 15);

        if ($count_pag <= 1) {
            return false;
        } else {
            parse_str($_SERVER['QUERY_STRING'], $vars);

            $pagination = '<nav aria-label="Page navigation"><ul class="pagination">';
            for ($i = 1; $i <= $count_pag; $i++) {
                $class = ($this->current_page == $i ? 'class="active"' : '');
                $vars['page'] = $i;
                $link = '?'.http_build_query($vars);
                $pagination .= '<li '.$class.'><a href="'.$link.'">'.$i.'</a></li>';
            }

            $pagination .= '</ul></nav>';

            return $pagination;
        }

    }

    /**
    * Add the CSS & JavaScript files you want to be loaded in the BO.
    */
    public function hookBackOfficeHeader()
    {
        $this->context->controller->addJS($this->_path.'views/js/jtgallert-back.js');
        $this->context->controller->addCSS($this->_path.'views/css/jtgallert-back.css');
    }

    /**
     * Add the CSS & JavaScript files you want to be added on the FO.
     */
    public function hookHeader()
    {
    }

    /**
     *
     */
    public function hookDisplayTop()
    {
        /* Place your code here. */
    }
}
