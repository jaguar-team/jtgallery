<?php
/**
 * Author: Vladislav Dneprov
 * Author URI: https://www.linkedin.com/in/vladislav-dneprov
 * Date: 20.12.2016 10:47
 * Version: 1.0.0
 */

class JtgalleryGalleryModuleFrontController extends ModuleFrontController
{
    public $display_column_left = false;

    public function initContent()
    {
        parent::initContent();
        $this->context->controller->addCSS($this->module->getPathUri().'views/css/jtgallery-front.css', 'all');
        $this->context->controller->addCSS($this->module->getPathUri().'views/css/unite-gallery.css', 'all');

        $this->context->controller->addCSS($this->module->getPathUri().'views/css/jquery.tiles-gallery.css', 'all');
        $this->context->controller->addJS($this->module->getPathUri().'views/js/jquery.tiles-gallery.js', 'all');

        $this->context->controller->addJS($this->module->getPathUri().'/views/js/jtgallery-front.js');
        $this->context->controller->addJS($this->module->getPathUri().'views/js/unitegallery.min.js', 'all');
        $this->context->controller->addJS($this->module->getPathUri().'views/js/ug-theme-tiles.js', 'all');
        $this->context->controller->addJS($this->module->getPathUri().'views/js/jtgallery-front.js', 'all');

        /** smarty var */
        $this->context->smarty->assign(array(
            'data'          => $this->getPhotos(),
        ));

        $this->setTemplate('gallery.tpl');
    }

    public function getPhotos()
    {
        $sql = 'SELECT * FROM '._DB_PREFIX_.'jtgallery';
        return Db::getInstance()->ExecuteS($sql);
    }
}